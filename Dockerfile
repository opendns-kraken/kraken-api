FROM nullfox/nodejs-zmq

WORKDIR /root

ADD package.json /root/package.json
RUN npm install

ADD bin /root/bin
ADD api /root/api
ADD config /root/config
ADD app.js /root/app.js

EXPOSE 3000

CMD ["npm", "start"]